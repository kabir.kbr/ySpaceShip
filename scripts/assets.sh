#!/bin/bash

# delete potentially sensitive data from configration files (if exists):
#echo "{}" > ./lss/configs/pbrain.conf
#echo "{}" > ./lss/resources/spaceGraphDataLast.dat
#echo "{}" > ./lss/resources/enableFiltersTemp.dat

# dependency directory
DEPEN="/opt/yspaceship/required"	

## geting required dependencies
if [ ! -d $DEPEN ]; then mkdir -p $DEPEN; fi
cd $DEPEN
# get java
if [ "`lsb_release -a 2>/dev/null | awk -F'#' 'NR==3' | awk '{print $2}'`" == "14.04" ]; then
	sudo add-apt-repository ppa:openjdk-r/ppa -y;
	sudo apt-get update;
fi;
sudo apt-get install openjdk-8-jdk -y
if [ ! -d java-8-openjdk-amd64 ]; then cp -r /usr/lib/jvm/java-8-openjdk-amd64 .; fi

if [ ! -d jre ]; then if [ ! -f jre-8u102-linux-x64.tar.gz ]; then wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" 'http://download.oracle.com/otn-pub/java/jdk/8u102-b14/jre-8u102-linux-x64.tar.gz'; fi; tar xf jre-8u102-linux-x64.tar.gz; rm jre-8u102-linux-x64.tar.gz; mv jre1.8.0_102 jre; fi
# get groovy
if [ ! -d groovy ]; then if [ ! -f apache-groovy-binary-2.4.6.zip ]; then wget "https://dl.bintray.com/groovy/maven/apache-groovy-binary-2.4.6.zip"; fi; unzip apache-groovy-binary-2.4.6.zip; mv groovy-2.4.6 groovy; rm apache-groovy-binary-2.4.6.zip; fi
#get titan
if [ ! -d titan-server-0.4.4 ]; then if [ ! -f titan-server-0.4.4.zip ]; then wget "http://s3.thinkaurelius.com/downloads/titan/titan-server-0.4.4.zip"; fi; unzip titan-server-0.4.4.zip; rm titan-server-0.4.4.zip; fi
#get elasticsearch
if [ ! -d elasticsearch-0.90.3 ]; then if [ ! -f elasticsearch-0.90.3.zip ]; then wget "https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-0.90.3.zip" ; fi; unzip elasticsearch-0.90.3.zip; rm elasticsearch-0.90.3.zip; fi
#get node binary
if [ ! -d node ]; then if [ ! -f node-v6.4.0-linux-x64.tar.xz ]; then wget "https://nodejs.org/dist/v6.4.0/node-v6.4.0-linux-x64.tar.xz" ; fi; tar xf node-v6.4.0-linux-x64.tar.xz; rm node-v6.4.0-linux-x64.tar.xz; mv node-v6.4.0-linux-x64 node; fi

#get chrome browser
CHROME_OK=$(dpkg-query -W --showformat='${Status}\n' google-chrome-stable)
if [ ! "$CHROME_OK" == "install ok installed" ]; then
	wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	sudo dpkg -i google-chrome*.deb
	sudo apt-get install -f
fi

## install selenium for headless tests
## according to here: http://www.cakelabs.lk/blog/quality-assurance/running-selenium-tests-in-headless-mode-on-jenkins-2/

# xvfb
sudo apt-get install xvfb -y

