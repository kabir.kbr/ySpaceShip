﻿# Windows launcher script for yspaceship
# author: Viktoras Veitas vveitas@gmail.com


if ($args[0] -eq "start") {
    $PDFLIBRARY_DEFAULT = Get-Content pdflibrary.conf
    $PDFLIBRARY = Read-Host 'Full PATH to pdf library in a form C:\path\to\library [default - ' $PDFLIBRARY_DEFAULT']'
    if ($PDFLIBRARY -eq "") { $PDFLIBRARY = $PDFLIBRARY_DEFAULT }
    if ($PDFLIBRARY -eq $null) { $PDFLIBRARY = $PDFLIBRARY_DEFAULT }
    $PDFLIBRARY | Out-File -Encoding "utf8" -FilePath "pdflibrary.conf" 
  

    if (-not (Test-Path "spacegraph")) {
	    New-Item -ItemType directory "spacegraph"
        cd spacegraph
	    New-Item -ItemType directory "logs"
        New-Item -ItemType directory "configs"
        New-Item -ItemType directory "resources"
        $EMPTY_JSON = "{}"
	    $EMPTY_JSON | Out-File -Encoding "utf8" -FilePath "configs\pbrain.conf"  
	    $EMPTY_JSON | Out-File -Encoding "utf8" -FilePath "resources\spaceGraphDataLast.dat"
	    $EMPTY_JSON | Out-File -Encoding "utf8" "resources\enableFiltersTemp.dat"
	    cd ..
    }
    if (-not (Test-Path spacegraph\elasticsearch)) {
	    New-Item -ItemType directory spacegraph\elasticsearch
    }

    $temp= Invoke-WebRequest 'https://registry.hub.docker.com/v2/repositories/vveitas/yspaceship/tags' | ConvertFrom-Json
    $LATEST_REMOTE= $temp.results.name[0]

    $LOCAL_EXISTS=docker images vveitas/yspaceship:$LATEST_REMOTE -q
    if ($LOCAL_EXISTS -eq "") {
        Write-Host -NoNewline "New ySpaceShip version " $LATEST_REMOTE "is available"
        Write-Host ""
        $UPDATE = Read-Host "Update/download? [y/N]"
        if (($UPDATE -eq "y") -or ($UPDATE -eq "Y")) {
            docker pull vveitas/yspaceship:$LATEST_REMOTE
        } 
    }

    $SCRIPT_PATH = $PSScriptRoot
    $CONTAINER_NAME= -join ((1..16) | %{(65..90)+(97..122) | Get-Random} | % {[char]$_})
    $CONTAINER_NAME | Out-File -Encoding "utf8" -FilePath "container.name"

    if ($args[1] -eq "debug") {
        Write-Host "running yspacheship in debug mode"
        docker run --name $CONTAINER_NAME -i -t -p 3000:3000 -p 3001:3001 -p 3002:3002 -v $SCRIPT_PATH/spacegraph:/opt/yspaceship/spacegraph -v ${PDFLIBRARY}:/opt/yspaceship/pdflibrary:ro vveitas/yspaceship:$LATEST_REMOTE /bin/bash
    }
    Else {
        Write-Host "Launching ySpaceShip..."
        docker run --name $CONTAINER_NAME -d -p 3000:3000 -p 3001:3001 -p 3002:3002 -v $SCRIPT_PATH/spacegraph:/opt/yspaceship/spacegraph -v ${PDFLIBRARY}:/opt/yspaceship/pdflibrary:ro vveitas/yspaceship:$LATEST_REMOTE sh ./scripts/shell/yspaceship.sh start
        Write-Host "...Done."
        Start-Sleep -s 2
    }


}

if ($args[0] -eq "stop") {
    if (Test-Path "container.name") {
        $CONTAINER_NAME = Get-Content container.name
        Write-Host "Stopping ySpaceShip.."
        docker stop $CONTAINER_NAME
        rm container.name
        Write-Host "...Done."
        Start-Sleep -s 1
    }
    Else {
        Write-Host "yspaceship container is not running... nothing to stop. if that is not the case, then the container name was lost on the way and you have to stop it manually..."
    }
}
