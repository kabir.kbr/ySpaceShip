Feature: Application settings

  Users should be able to set and save application settings needed for 
  it to run;

  Background:
    Given application settings modal is displayed

  Scenario Outline: Input and save settings with defaults
    When user inputs settings <zotero_key>, <zotero_userID> and <zotero_collectionID>
    And presses save button 
    Then dialog window closes
    And file <pbrain.conf> contains application and default settings

  Examples: test application settings
      | zotero_key               | zotero_userID | zotero_collectionID |  
      | OdCU3l6dwtWpbOxiRRb2Naij | 3075376       | JVBN3XKB            | 

  Scenario Outline: Input and save all settings
    When user selects <local_pdf_editor> as <Linux(Okular)>
    And presses save button 
    Then dialog window closes
    And file <pbrain.conf> has <local_pdf_editor> value <Linux(Okular)>

  Examples: default settings
      | local_pdf_editor      | pdf_library_dir                                 |  
      | PDF-XChange (Windows) | /home/vveitas/vveitas@gmail.com/pbrain-library/ |  

  Scenario: Close dialog without saving
    When user presses close button without save
    Then dialog window closes
    And file pbrain.conf remains empty

