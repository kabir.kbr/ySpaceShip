import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.PendingException;

public class ApplicationSettingsStepDefs {

    @Given("^application settings modal is displayed$")
    public void application_settings_modal_is_displayed() throws Throwable {
        throw new PendingException();
    }

    @When("^user inputs settings (.+), (.+) and (.+)$")
    public void user_inputs_settings_and(String zoterokey, String zoterouserid, String zoterocollectionid) throws Throwable {
        throw new PendingException();
    }

    @When("^user selects (.+) as (.+)$")
    public void user_selects_as(String localpdfeditor, String linuxokular) throws Throwable {
        throw new PendingException();
    }

    @When("^user presses close button without save$")
    public void user_presses_close_button_without_save() throws Throwable {
        throw new PendingException();
    }

    @Then("^dialog window closes$")
    public void dialog_window_closes() throws Throwable {
        throw new PendingException();
    }

    @And("^presses save button $")
    public void presses_save_button() throws Throwable {
        throw new PendingException();
    }

    @And("^file (.+) contains application and default settings$")
    public void file_contains_application_and_default_settings(String filename) throws Throwable {
        throw new PendingException();
    }

    @And("^file (.+) has (.+) value (.+)$")
    public void file_has_value(String pbrainconf, String localpdfeditor, String linuxokular) throws Throwable {
        throw new PendingException();
    }

    @And("^file (.+) remains empty$")
    public void file_pbrainconf_remains_empty(String filename) throws Throwable {
        throw new PendingException();
    }
}