
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.PendingException;
import cucumber.api.Scenario;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

import java.io.File;
import org.apache.commons.io.FileUtils;

import static org.junit.Assert.assertEquals;

public class ApplicationManagementWindowsStepDefs {
    ChromeOptions chromeOptions;
    WebDriver driver;
    private Scenario thisScenario;

    @Before
    public void before(Scenario scenario) {
        thisScenario = scenario;
        System.setProperty("webdriver.chrome.driver", "./webdrivers/chromedriver");
        chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--verbose", "--ignore-certificate-errors");
        driver = new ChromeDriver();
    }

    @Given("^the ySpaceShip is running$") 
    public void the_yspaceship_is_running() throws Throwable {
        driver.get("http://localhost:3000");
        String actualTitle = driver.getTitle();
        String expectedTitle = "ySpaceShip";
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertEquals(actualTitle, expectedTitle);        
    }

    @When("^user presses gear icon$")
    public void user_presses_gear_icon() throws Throwable {
        throw new PendingException();
    }

    @Then("^application settings dialogue is opened$")
    public void application_settings_dialogue_is_opened() throws Throwable {
        throw new PendingException();
    }

    @After
    public void after() {
        driver.close();
        driver.quit();
    }

}
