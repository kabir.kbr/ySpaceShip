# Getting info from Zotero remotely

## Get all tags

* Load configuration from configuration "pbrain-test.conf"
* Get all tags from remote Zotero repository
* There are "3" tags in result
* Following tags are found
    |TagName |
    |--------|
    |tag1    |
    |tag2    |
    |tag3    |
