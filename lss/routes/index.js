var express = require('express');
var router = express.Router();
var jsonfile = require('jsonfile');
var JSONStream = require('JSONStream');
var fs = require('fs');
var child_process = require('child_process');
var wait = require('wait.for');
var dateFormat = require('dateformat');
var now = require('performance-now');


module.exports = router;

var database = require('../configs/database');
var settingsFile = './configs/pbrain.conf';
var zoteroItemsFile = './resources/zoteroItems.dat';
var bibtexItemsFile = './resources/bibtexItems.dat';
var settings = jsonfile.readFileSync(settingsFile);
var filterStatus = 'none';

var spaceGraphData = jsonfile.readFileSync('./resources/spaceGraphDataLast.dat');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'ySpaceShip',
                        tagsArray: spaceGraphData['matchingTags'],
                        authorsArray: spaceGraphData['matchingAuthors'],
                        publicationsArray: spaceGraphData['matchingPublications'],
                        pdfFilesArray: spaceGraphData['matchingPDFfiles'],
                        annotationsArray: spaceGraphData['matchingAnnotations'],
                        settings: settings,
                        toggleModal: req.session.toggleModal,
                        fullTextSearchFields: database.fullTextSearchFields,
                        synchronizationStepArray: database.synchronizationStepArray,
                        filterStatus: filterStatus
                      });

});

router.post('/save-settings', function(req, res, next) {
    wait.launchFiber(saveSettingsFile, req, res);
    settings = jsonfile.readFileSync(settingsFile);
    res.redirect("/");
});

function saveSettingsFile(req,res) {
  wait.for(function() {jsonfile.writeFileSync(settingsFile, req.body)});
}

/* socket server for communication with the browser */
var server = express().listen(process.env.PORT || 3001);
var io = require('socket.io')(server);
io.on('connection', function(socket) {
    socket.on('synchronizeSpaceGraph', function() {
        wait.launchFiber(synchronizeSpaceGraph);
    })

})

/* socket server for communication with the java */
//var serverJava = express().listen(process.env.PORT || 3003);
//var ioJava = require('socket.io')(serverJava);


router.post('/enableFilters', function(req,res) {
  // get all settings from DOM
  // run groovy script on calculating subgraph;
  // get publicaitons and annotations from subgraph;
  // and return to redirect all.

  deleteFileContents("./logs/rolling.log"); // delete old log file
  var data = req.body;
  groovyScriptEnableFilters(data,req,res);
  filterStatus = "block";
});

router.post('/disableFilters',function(req,res) {
  spaceGraphData = jsonfile.readFileSync('./resources/spaceGraphDataLast.dat');
  filterStatus = "none";
  res.send({ result: "success"});
});

router.post('/clearSpaceGraph', function(req,res) {
  groovyScriptClearSpaceGraph(req,res);
});

router.post('/updateSinglePublication', function(req,res) {
  var data = req.body;
  console.log(data);
  groovyScriptUpdateSinglePublication(data, res);
});

function deleteFileContents(relativeFilePath) {
    fs.writeFile(relativeFilePath, "", function(err) {
    if(err) {
          return console.log(err);
      }
  });
}

function groovyScriptUpdateSinglePublication(data, res) {
  var args = new Array(JSON.stringify(data.itemKey));
  console.log(getClass(args));
  console.log('arguments received in index.js: '+args);
  //var enableFilters = child_process.spawnSync("./scripts/shell/enableFilters.sh", args);
  var updateSinglePublication = child_process.execFile('./scripts/shell/updateSinglePublication.sh', args);

  updateSinglePublication.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
    //spaceGraphData = JSON.parse(data);
  });

  updateSinglePublication.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
  });

  updateSinglePublication.on('close', (code) => {
    console.log('child process updateSinglePublication exited with code ${code}');
    groovyScript({name: 'update-single-publication', function:'updateSinglePublication',result:'done'});
    spaceGraphData = jsonfile.readFileSync('./resources/spaceGraphDataLast.dat');
    res.redirect('/');
  });

}

function groovyScriptEnableFilters(data) {
  var args = [ JSON.stringify(data) ];
  var start = now();
  var enableFilters = child_process.execFile('./scripts/shell/enableFilters.sh', args);

  enableFilters.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
    //spaceGraphData = JSON.parse(data);
  });

  enableFilters.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
  });

  enableFilters.on('close', (code) => {
    console.log('Child process enableFilters exited.');
    spaceGraphData = jsonfile.readFileSync('./resources/enableFiltersTemp.dat');
    io.emit('enableFilters', { status: 'success'});
    var finish = now();
    console.log("Took "+(finish-start)/1000+" seconds to complete.");
  });

}

function synchronizeSpaceGraph() {
  deleteFileContents("./logs/rolling.log"); // delete old log file
  var synchronizationStepArray = database.synchronizationStepArray;
  for (var i = 0; i < synchronizationStepArray.length; i++) {
      var synchronizationStep = synchronizationStepArray[i];
      //wait.for(functions[synchronizationStep.function], synchronizationStep, req, res);
      wait.for(groovyScript, synchronizationStep);
  }

  timeout.unref();
}

function groovyScriptClearSpaceGraph(req,res) {
  //var enableFilters = child_process.spawnSync("./scripts/shell/enableFilters.sh", args);
  var clearSpaceGraph = child_process.execFile('./scripts/shell/clearSpaceGraph.sh');

  clearSpaceGraph.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
    //spaceGraphData = JSON.parse(data);
  });

  clearSpaceGraph.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
  });

  clearSpaceGraph.on('close', (code) => {
    console.log('child process enableFilters exited with code ${code}');
    spaceGraphData = jsonfile.readFileSync('./resources/spaceGraphDataLast.dat');
    res.redirect("/");
  });
}

var timeout = setTimeout(saveSettingsFile,2147483647); // this sets timeout for 25 days, which is crazy, but do not know what to do with it rith now...

function groovyScript(step, callback ) {
  //var processData = child_process.exec('./scripts/shell/'+step.function+'.sh');
  var processData = child_process.execFile('./scripts/shell/'+step.function+'.sh');
  processData.stdout.on('data', (data) => {
    //console.log(`stdout: ${data}`);
    if (step.name == 'loading-spacegraph') {
      spaceGraphData = jsonfile.readFileSync('./resources/spaceGraphDataLast.dat');
    }
  });

  processData.stderr.on('data', (data) => {
    //console.log(`stderr: `+JSON.parse(data));
    io.emit('progress-info', JSON.parse(data));
  });

  processData.on('close', (code) => {
    console.log('child process '+step.function +' exited with code ${code}');
    io.emit('synchronization-step', { name: step.name,
                                      result: 'success',
                                      report: step.report
                                    }
    );
    if (step.name == 'loading-spacegraph') {
      io.emit('synchronizeSpaceGraph',{status: 'finished'});
    }
    callback(null,"closed");
  });

}

// little helper function for getting a class of the (any) object;
function getClass(obj) {
  if (typeof obj === "undefined")
    return "undefined";
  if (obj === null)
    return "null";
  return Object.prototype.toString.call(obj)
    .match(/^\[object\s(.*)\]$/)[1];
}
