#!/bin/sh

export JAVA_HOME=/opt/yspaceship/required/jre
export GROOVY_HOME=/opt/yspaceship/required/groovy
export PATH=$GROOVY_HOME/bin:$PATH
export TITAN_HOME=/opt/yspaceship/required/titan-server-0.4.4

# groovy -cp scripts/groovy scripts/groovy/extractAnnotations.groovy
java -cp "scripts/java:/$GROOVY_HOME/lib/*:/$TITAN_HOME/lib/*" ExtractPDFAnnotations

exit 0
