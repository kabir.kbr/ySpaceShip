@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import groovy.transform.TypeChecked

class KillProcess extends TimerTask {
  Process p;
  static Logger logger

  @TypeChecked
  KillProcess(Process p) {
    def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
    PropertyConfigurator.configure(config.toProperties())
    logger = LoggerFactory.getLogger('KillProcess.class');
    this.p = p;
  }

  @Override
  @TypeChecked
  public void run() {
      logger.warn("Timeout exceeded -- killing the process.")
      this.p.destroy();
  }
}
