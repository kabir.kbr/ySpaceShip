**ySpaceShip** (pronounced "why spaceship?") is a cross-platform uber tool for academic research and beyond. It integrates Zotero bibliography database and annotated PDF files in single full-text searchable database.  ySpaceShip is a part of the [Academic Collaboration Framework](https://sites.google.com/site/scientificcollaboration/).   

The application is distributed as a docker container with node.js frontend. Current status can be considered as an early public beta, therefore not all (desired) functionality is yet available and a bunch of bugs are. We are collecting [feature and bug requests](https://gitlab.com/vveitas/pbrain-modular/issues) and moving towards full release.

[![](https://images.microbadger.com/badges/image/vveitas/yspaceship.svg)](http://microbadger.com/images/vveitas/yspaceship "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/version/vveitas/yspaceship.svg)](http://microbadger.com/images/vveitas/yspaceship "Get your own version badge on microbadger.com")

ySpaceShip can be used both locally an in server environment.

#### Screenshot #1

<img src="https://gitlab.com/vveitas/ySpaceShip/raw/master/screenshots/yspaceship-0.0.2-publications.png" width="600">

#### Screenshot #2

<img src="https://gitlab.com/vveitas/ySpaceShip/raw/master/screenshots/yspaceship-0.0.2-annotations.png" width="600">
